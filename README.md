Repozytoria Styli, Symboli, Palet oraz innych pomocy dla QGIS

# Style dla QGIS 2.6+ #

## bdot_symbolizacje ##

Zbiór symboli SVG odpowiadających wzorom z rozporządzenia BDOT. 
Jednocześnie, dostosowane do zmiany kolorów obrysu i wypełnienia bezpośrednio z poziomu QGIS.

# Usługi OGC #

## WMS ##

Lista polskich usług WMS dla QGIS. Najbardziej przydatne adresy serwisów.